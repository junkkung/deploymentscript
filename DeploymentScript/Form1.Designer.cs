﻿namespace DeploymentScript
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.destinationBrowseBt = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.destinationTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Title = new System.Windows.Forms.Label();
            this.sourceBrowseBt = new System.Windows.Forms.Button();
            this.sourceTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.generateBt = new System.Windows.Forms.Button();
            this.deployProgramName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.deployDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // destinationBrowseBt
            // 
            this.destinationBrowseBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.destinationBrowseBt.Location = new System.Drawing.Point(876, 207);
            this.destinationBrowseBt.Margin = new System.Windows.Forms.Padding(4);
            this.destinationBrowseBt.Name = "destinationBrowseBt";
            this.destinationBrowseBt.Size = new System.Drawing.Size(108, 36);
            this.destinationBrowseBt.TabIndex = 0;
            this.destinationBrowseBt.Text = "Browse";
            this.destinationBrowseBt.UseVisualStyleBackColor = true;
            this.destinationBrowseBt.Click += new System.EventHandler(this.destinationBrowseBt_Click);
            // 
            // destinationTextBox
            // 
            this.destinationTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.destinationTextBox.Location = new System.Drawing.Point(36, 207);
            this.destinationTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.destinationTextBox.Multiline = true;
            this.destinationTextBox.Name = "destinationTextBox";
            this.destinationTextBox.Size = new System.Drawing.Size(831, 35);
            this.destinationTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Helvetica", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 180);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(286, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Destination directory (existing)";
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Maiandra GD", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(28, 26);
            this.Title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(470, 41);
            this.Title.TabIndex = 2;
            this.Title.Text = "Deployment script generator";
            // 
            // sourceBrowseBt
            // 
            this.sourceBrowseBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sourceBrowseBt.Location = new System.Drawing.Point(876, 292);
            this.sourceBrowseBt.Margin = new System.Windows.Forms.Padding(4);
            this.sourceBrowseBt.Name = "sourceBrowseBt";
            this.sourceBrowseBt.Size = new System.Drawing.Size(108, 36);
            this.sourceBrowseBt.TabIndex = 0;
            this.sourceBrowseBt.Text = "Browse";
            this.sourceBrowseBt.UseVisualStyleBackColor = true;
            this.sourceBrowseBt.Click += new System.EventHandler(this.sourceBrowseBt_Click);
            // 
            // sourceTextBox
            // 
            this.sourceTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sourceTextBox.Location = new System.Drawing.Point(36, 292);
            this.sourceTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.sourceTextBox.Multiline = true;
            this.sourceTextBox.Name = "sourceTextBox";
            this.sourceTextBox.Size = new System.Drawing.Size(831, 35);
            this.sourceTextBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Helvetica", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(31, 265);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(283, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Source directory (new deploy)";
            // 
            // generateBt
            // 
            this.generateBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.generateBt.Location = new System.Drawing.Point(426, 349);
            this.generateBt.Margin = new System.Windows.Forms.Padding(4);
            this.generateBt.Name = "generateBt";
            this.generateBt.Size = new System.Drawing.Size(188, 87);
            this.generateBt.TabIndex = 0;
            this.generateBt.Text = "Generate";
            this.generateBt.UseVisualStyleBackColor = true;
            this.generateBt.Click += new System.EventHandler(this.generateBt_Click);
            // 
            // deployProgramName
            // 
            this.deployProgramName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deployProgramName.Location = new System.Drawing.Point(36, 119);
            this.deployProgramName.Margin = new System.Windows.Forms.Padding(4);
            this.deployProgramName.Multiline = true;
            this.deployProgramName.Name = "deployProgramName";
            this.deployProgramName.Size = new System.Drawing.Size(439, 35);
            this.deployProgramName.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Helvetica", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(33, 92);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "Deploy program name";
            // 
            // deployDate
            // 
            this.deployDate.Location = new System.Drawing.Point(499, 119);
            this.deployDate.Margin = new System.Windows.Forms.Padding(4);
            this.deployDate.Name = "deployDate";
            this.deployDate.Size = new System.Drawing.Size(368, 22);
            this.deployDate.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Helvetica", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(493, 91);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 24);
            this.label4.TabIndex = 2;
            this.label4.Text = "Deploy date";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1016, 452);
            this.Controls.Add(this.deployDate);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.deployProgramName);
            this.Controls.Add(this.sourceTextBox);
            this.Controls.Add(this.generateBt);
            this.Controls.Add(this.sourceBrowseBt);
            this.Controls.Add(this.destinationTextBox);
            this.Controls.Add(this.destinationBrowseBt);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button destinationBrowseBt;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox destinationTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Button sourceBrowseBt;
        private System.Windows.Forms.TextBox sourceTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button generateBt;
        private System.Windows.Forms.TextBox deployProgramName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker deployDate;
        private System.Windows.Forms.Label label4;
    }
}

