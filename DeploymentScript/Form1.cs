﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace DeploymentScript
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void destinationBrowseBt_Click(object sender, EventArgs e)
        {
            ChooseFolder(destinationTextBox);
        }

        private void sourceBrowseBt_Click(object sender, EventArgs e)
        {
            ChooseFolder(sourceTextBox);
        }

        public void ChooseFolder(TextBox textbox)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textbox.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void generateBt_Click(object sender, EventArgs e)
        {
            List<string> destinationList = getAllFile(destinationTextBox);
            List<string> sourceList = getAllFile(sourceTextBox);

            if (destinationTextBox.Text == null || destinationTextBox.Text == "")
            {
                MessageBox.Show("Please select destination directory", "Error", MessageBoxButtons.OK);
                return;
            }

            if (sourceTextBox.Text == null || sourceTextBox.Text == "")
            {
                MessageBox.Show("Please select source directory", "Error", MessageBoxButtons.OK);
                return;
            }

            Dictionary<string, List<string>> comparedFiles = compareFile(destinationList, sourceList);
            generateDeployScript(comparedFiles);
        }

        private string readFilesFromDirectory (string directoryName)
        {
            string result;
            System.Diagnostics.Process si = new System.Diagnostics.Process();
            si.StartInfo.WorkingDirectory = @"" + directoryName;
            si.StartInfo.UseShellExecute = false;
            si.StartInfo.FileName = "cmd.exe";
            si.StartInfo.Arguments = "/c dir /s /b /a-d";
            si.StartInfo.CreateNoWindow = true;
            si.StartInfo.RedirectStandardInput = true;
            si.StartInfo.RedirectStandardOutput = true;
            si.StartInfo.RedirectStandardError = true;
            si.Start();
            result = si.StandardOutput.ReadToEnd();
            si.Close();

            return result;
        }

        private List<string> getAllFile(TextBox textBox)
        {
            string[] rawDirectory = readFilesFromDirectory(textBox.Text).Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            List<string> files = new List<string>();
            if (rawDirectory.Length > 0)
            {
                for (int i = 0; i < rawDirectory.Length; i++)
                {
                    if (rawDirectory[i] != null && rawDirectory[i] != "")
                    {
                        string splitReferenceDirectory = rawDirectory[i].Remove(0, (textBox.Text.Length + 1));
                        files.Add(splitReferenceDirectory);
                    }
                }
            }
            return files;
        }

        private Dictionary<string, List<string>> compareFile(List<string> destinationList, List<string> sourceList)
        {
            Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();
            List<string> updateList = destinationList.Where(d => sourceList.Any(s => d == s)).ToList();
            List<string> deleteList = destinationList.Where(d => !sourceList.Any(s => d == s)).ToList();
            List<string> addNewList = sourceList.Where(s => !destinationList.Any(d => s == d)).ToList();

            result.Add("update", updateList);
            result.Add("delete", deleteList);
            result.Add("new", addNewList);
            return result;
        }

        private void generateDeployScript (Dictionary<string, List<string>> comparedFiles)
        {
            try
            {
                string currentPath = Directory.GetCurrentDirectory();
                string buildPath = Path.Combine(currentPath, deployProgramName.Text);
                string patchPath = Path.Combine(buildPath, "patch");
                string deploymentToolText = "Deployment_Tool.exe";

                // check existing build folder
                if (Directory.Exists(buildPath))
                {
                    DirectoryInfo di = new DirectoryInfo(buildPath);
                    di.Delete(true);
                }

                // prepare directory
                Directory.CreateDirectory(buildPath);
                Directory.CreateDirectory(patchPath);

                // copy deployment_tool
                File.Copy(Path.Combine(currentPath, deploymentToolText), Path.Combine(buildPath, deploymentToolText), true);

                // copy source file
                foreach (string dirPath in Directory.GetDirectories(sourceTextBox.Text, "*", SearchOption.AllDirectories))
                {
                    Directory.CreateDirectory(dirPath.Replace(sourceTextBox.Text, patchPath));
                }

                // change pointer to build folder
                Directory.SetCurrentDirectory(buildPath);

                int fileCount = comparedFiles["update"].Count + comparedFiles["delete"].Count + comparedFiles["new"].Count;

                StreamWriter streamWriter = new StreamWriter("MigrationScript.txt");
                streamWriter.WriteLine("[INFO]");
                streamWriter.WriteLine("SystemName=" + deployProgramName.Text);
                streamWriter.WriteLine("Build=" + deployDate.Value.ToString("yyyyMMdd"));
                streamWriter.WriteLine("BaseDestinationPath=" + destinationTextBox.Text);
                streamWriter.WriteLine("BaseSourcePath=.\\patch");
                streamWriter.WriteLine("");
                streamWriter.WriteLine("[DEPLOYMENT]");
                streamWriter.WriteLine("FileCount=" + fileCount.ToString());
                if (comparedFiles["update"].Count > 0)
                {
                    for (int i = 0; i < comparedFiles["update"].Count; i++)
                    {
                        streamWriter.WriteLine("File" + (i + 1).ToString() + "=" + comparedFiles["update"][i] + ",O,,," + "U");
                    }
                }

                int startPosition = comparedFiles["update"].Count;
                if (comparedFiles["delete"].Count > 0)
                {
                    for (int i = 0; i < comparedFiles["delete"].Count; i++)
                    {
                        streamWriter.WriteLine("File" + (i + 1 + startPosition).ToString() + "=" + comparedFiles["delete"][i] + ",O,,," + "R");
                    }
                }

                startPosition = comparedFiles["update"].Count + comparedFiles["delete"].Count;
                if (comparedFiles["new"].Count > 0)
                {
                    for (int i = 0; i < comparedFiles["new"].Count; i++)
                    {
                        streamWriter.WriteLine("File" + (i + 1 + startPosition).ToString() + "=" + comparedFiles["new"][i] + ",O,,," + "A");
                    }
                }

                streamWriter.WriteLine("");
                streamWriter.WriteLine("[EXAMPLE]");
                streamWriter.WriteLine("File+RunningNumber=FileName,[O=Overwrite I=Ignore],Source Folder,Destination Folder,[A=Add New,U=Update,R=Remove]");
                streamWriter.Close();

                MessageBox.Show("Migration script generated", "Success", MessageBoxButtons.OK);
            } 
            catch (Exception ex)
            {
                MessageBox.Show("There is some error occur: " + ex.ToString(), "Error", MessageBoxButtons.OK);
            }           
        }
    }
}
